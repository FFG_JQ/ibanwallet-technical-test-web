import styled, { css } from 'styled-components'

const Input = styled.input`
  border: 1px solid #afafaf;
  font-family: CircularStd-Book;
  background-color: transparent;
  width: 540px;
  height: 60px;
  font-size: 18px;
  line-height: 24px;
  padding: 0px 18px;
  margin: 18px 0px;
  color: #c8c8c8;
  ::placeholder {
    color: inherit;
  }
`

export default Input
