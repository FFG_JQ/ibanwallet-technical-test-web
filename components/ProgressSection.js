import styled, { css } from 'styled-components'
import AnchorLink from './AnchorLink'
import Link from 'next/link'

const TopDash = styled("div")`
  margin-bottom: 36px;
`
const ProgressWrapper = styled("div")`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: space-around;
`
const ProgressBar = styled("div")`
  flex: 5;
  border: 1px solid #e6e6e6;
  border-radius: 100px;
  height: 6px;
`

const ProgressAmount = styled("div")`
  background-color: #00ce7c;
  border-radius: 100px;
  height:100%;
  width:${props => props.percent};
`

const CurrentTopic = styled("p")`
  margin: 0 auto;
  font-family: CircularStd-Book;
  font-size: 12px;
  margin-left: 70px;
  color: #838383;
`
const CancelButton = styled(AnchorLink)`
  flex:1;
`
const ProgressSection = (props) => (
  <TopDash>
    <ProgressWrapper>
      <ProgressBar>
        <ProgressAmount percent={props.percent}/>
      </ProgressBar>
      <Link href='#'>
        <CancelButton>Cancel</CancelButton>
      </Link>
    </ProgressWrapper>
    <CurrentTopic>{props.topic}</CurrentTopic>
  </TopDash>
);
export default ProgressSection
