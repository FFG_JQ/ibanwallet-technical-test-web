import styled, { css } from 'styled-components'

const AnchorLink = styled("a")`
  width: 50px;
  height: 18px;
  object-fit: contain;
  font-family: CircularStd;
  font-size: 14px;
  font-weight: bold;
  font-style: normal;
  font-stretch: normal;
  line-height: normal;
  letter-spacing: normal;
  text-align: right;
  color: #02ce7c;
`

export default AnchorLink;
