import styled, { css } from 'styled-components'

const Button = styled.button`
  border: 2px solid #c8c8c8;
  font-family: CircularStd-Bold;
  background-color: transparent;
  width: 240px;
  height: 64px;
  font-size: 18px;
  color: #c8c8c8;
  ${props =>
  props.valid &&
  css`
    border: 2px solid #00ce7c;
    background-color: #00ce7c;
    color: #ffffff;
    box-shadow: 0px 20px 1 0 #00ce7c;
  `
  }
`

export default Button
