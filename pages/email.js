import { Component } from 'react'
import Router from 'next/router'
import Link from 'next/link'
import Button from '../components/Button'
import AnchorLink from '../components/AnchorLink'
import ProgressSection from '../components/ProgressSection'

// import { connect } from "react-redux"
// import { saveEmail } from "../redux/actions"

import styled from 'styled-components'

const Wrapper = styled("div")`
  display: flex;
  padding: 70px 90px 130px 60px;
  border: 1px solid black;
  flex-direction: column;
`
const Title = styled("h1")`
  font-family: CircularStd-Black;
  font-size:36px;
  line-height: 34px;
  letter-spacing: 0;
  color: #0a0f35;
  margin: 20px 0px;
`
const SubTitle = styled("h2")`
  font-family: CircularStd-Book;
  font-size:18px;
  font-weight: normal;
  line-height: 20px;
  letter-spacing: 0;
  color: #838383;
  margin: 10px 0px 100px 0px;
`
const Text = styled("p")`
  font-family: CircularStd-Book;
  font-size:16px;
  font-weight: normal;
  line-height: 20px;
  letter-spacing: 0;
  color: #c8c8c8;
  margin: 0px 8px;
`
const Form = styled("form")`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  height: 600px;
`
const Input = styled.input`
  border: 1px solid #afafaf;
  font-family: CircularStd-Book;
  background-color: transparent;
  width: 540px;
  height: 60px;
  font-size: 18px;
  line-height: 24px;
  padding: 0px 18px;
  margin: 18px 0px;
  color: #c8c8c8;
  ::placeholder {
    color: inherit;
  }
`
const FlexBoxRow = styled("div")`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
`
class Email extends Component {
  constructor(props) {
    super(props)
    this.state = {
      valid:false
    }
  }

  onKeyPress = (e) => {
    if(e.key == 'Enter'){
      this.handleSubmit(e);
    }
  }

  handleChange = (e) => {
    const email = e.target.value;
    let dotTwoCharsValid = false;

    const atSymbolValid = email.includes('@');
    const dotPosition = email.indexOf('.');
    if(dotPosition != -1) {
      if(dotPosition <= email.length-3) {
        dotTwoCharsValid = true;
      }
    }
    this.setState({
      valid:(atSymbolValid && dotTwoCharsValid),
      email:e.target.value
    });
  }

  handleSubmit = (e) => {
    e.preventDefault();
    console.log("Submitted");
    if(this.state.valid === true) {
      //TODO: Add Redux Store Action Save here
      Router.push('/phone');
    }
  }
  render () {
    return (
      <Wrapper>
        <ProgressSection topic={'Email Address'} percent={'33%'}/>
        <Title>What's Your Email Address?</Title>
        <SubTitle>We'll send you a confirmation email and you'll use it to log in later.</SubTitle>
          <Form onSubmit={this.handleSubmit} onKeyPress={this.onKeyPress}>
            <Input width='220px' type="email" placeholder="e.g. jane.doe@email.com" name="email" onChange={this.handleChange}/>
            <FlexBoxRow>
              <AnchorLink>Previous</AnchorLink>
              <FlexBoxRow>
                <Text>Press Enter or</Text>
                <Button type='submit' valid={this.state.valid}>Continue</Button>
              </FlexBoxRow>
            </FlexBoxRow>
          </Form>
      </Wrapper>
    )
  }
}
export default Email;
