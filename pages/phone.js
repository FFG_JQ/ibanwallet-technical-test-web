import { Component } from 'react'
import Router from 'next/router'
import Link from 'next/link'
import Button from '../components/Button'
import AnchorLink from '../components/AnchorLink'
import ProgressSection from '../components/ProgressSection'

import styled from 'styled-components'

const Wrapper = styled("div")`
  display: flex;
  padding: 70px 90px 130px 60px;
  border: 1px solid black;
  flex-direction: column;
`
const Title = styled("h1")`
  font-family: CircularStd-Black;
  font-size:36px;
  line-height: 34px;
  letter-spacing: 0;
  color: #0a0f35;
  margin: 20px 0px;
`
const SubTitle = styled("h2")`
  font-family: CircularStd-Book;
  font-size:18px;
  font-weight: normal;
  line-height: 20px;
  letter-spacing: 0;
  color: #838383;
  margin: 10px 0px 100px 0px;
`
const Text = styled("p")`
  font-family: CircularStd-Book;
  font-size:16px;
  font-weight: normal;
  line-height: 20px;
  letter-spacing: 0;
  color: #c8c8c8;
  margin: 0px 8px;
`
const Form = styled("form")`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  height: 600px;
`
const FlexBoxRow = styled("div")`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
`
const Select = styled('select')`
  margin: 12px 12px 12px 0px;
  width:120px;
  height:60px;
  border: 1px solid #afafaf;
  border-radius: 0;
  appearance: none;
  background-color: transparent;
  font-family: CircularStd-Book;
  font-size: 18px;
  line-height: 24px;
  background-image: url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAAh0lEQVQ4T93TMQrCUAzG8V9x8QziiYSuXdzFC7h4AcELOPQAdXYovZCHEATlgQV5GFTe1ozJlz/kS1IpjKqw3wQBVyy++JI0y1GTe7DCBbMAckeNIQKk/BanALBB+16LtnDELoMcsM/BESDlz2heDR3WePwKSLo5eoxz3z6NNcFD+vu3ij14Aqz/DxGbKB7CAAAAAElFTkSuQmCC');
  background-repeat: no-repeat;
  background-position: 90px center;
  padding-left: 10px;
`
const Label = styled('label')`
  display: flex;
  flex-direction: column;
  font-family:CircularStd-Book;
  font-size:16px;
  color: #afafaf;
`
const InputSection = styled('FlexBoxRow')`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: flex-start;
`
const Input = styled.input`
  border: 1px solid #afafaf;
  font-family: CircularStd-Book;
  background-color: transparent;
  width: 220px;
  height: 60px;
  font-size: 18px;
  line-height: 24px;
  padding: 0px 18px;
  margin: 18px 0px;
  color: #c8c8c8;
  ::placeholder {
    color: inherit;
  }
`
class Phone extends Component {
  constructor(props) {
    super(props)
    this.state = {
      valid:false,
      countryCode:'',
      number:''
    }
  }

  onKeyPress = (e) => {
    if(e.key == 'Enter'){
      this.handleSubmit(e);
    }
  }

  handleChange = (e) => {
    let countryCode = e.target.name == 'countryCode' ? e.target.value : this.state.countryCode;
    let number = e.target.name == 'number' ? e.target.value : this.state.number;

    let countryCodeFilled = countryCode != '';

    let phoneNumberMinLength = (countryCode+number).length >= 9;

    this.setState({
      valid:(countryCodeFilled&&phoneNumberMinLength),
      [e.target.name]: e.target.value
    });
  }

  handleSubmit = (e) => {
    e.preventDefault();
    if(this.state.valid === true) {
      //TODO: Add Redux Store Action Save here
      Alert.alert('Submission Successful');
    }
  }
  render () {
    return (
      <Wrapper>
        <ProgressSection topic={'Phone Registration'} percent={'50%'}/>
        <Title>What's Your Phone Number?</Title>
        <SubTitle>Your number will be used to link your account and log into the app.</SubTitle>
          <Form onSubmit={this.handleSubmit} onKeyPress={this.onKeyPress}>
            <InputSection>
              <Label>
                Country
                <Select name="countryCode" onChange={this.handleChange}>
                  <option value=""></option>
                  <option value="+351" label="+351">+351</option>
                  <option value="+34" label='+34'>+34</option>
                </Select>
              </Label>
              <Label>
                Phone Number
                <Input type="text" name="number" onChange={this.handleChange}/>
              </Label>
            </InputSection>
            <FlexBoxRow>
              <AnchorLink>Previous</AnchorLink>
              <FlexBoxRow>
                <Text>Press Enter or</Text>
                <Button type='submit' valid={this.state.valid}>Continue</Button>
              </FlexBoxRow>
            </FlexBoxRow>
          </Form>
      </Wrapper>
    )
  }
}

export default Phone
